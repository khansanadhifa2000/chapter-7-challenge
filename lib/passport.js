const passport = require('passport');
// const localStrategy = require('passport-local').Strategy
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const {players} = require('../models')

async function authenticate(username, password, done) {
    try{
        const user = await players.authenticate({username, password})

        return done(null, user)
    }catch(e){
        return done(null, false, {
            message: e
        })
    }
}

passport.use(
    new localStrategy({usernameField: 'username', passwordField: 'password'}, authenticate)
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await players.findByPk(id))
)

module.exports = passport