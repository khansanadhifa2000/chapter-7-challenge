const {players} = require('../models')

module.exports = {
    dashboard: (req, res) => {
        players.findAll()
            .then(players => {
                res.render('dashboard', {players})
            })
        
    }
}