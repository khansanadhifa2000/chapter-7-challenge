module.exports = {
    chapter3: (req, res) => {
        res.render('chapter3')
    },
    registerForm: (req, res) => {
        res.render('register')
    },
    loginForm: (req, res) => {
        res.render('login')
    },
}