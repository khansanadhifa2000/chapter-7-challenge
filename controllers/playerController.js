const {players, players_biodata, players_history} = require("../models")

const profile = async (req, res, next) => {
    console.log(req.params.id)
    try{
        const player = await players.findOne({
            where: { id: req.params.id}
        });
        // const player = {players};
        
        await res.render('profile', {player});
    } catch (err) {
        next(err);
    }
};

const updateForm = async (req, res, next) => {
    try{
        const player = await players.findOne({
            where: { id: req.params.id}
        });
        // const player = {players};
        
        await res.render('update', {player});
    } catch (err) {
        next(err);
    }
};

const update = async (req, res, next) => {
    try{
        const player = await players.update({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        }, {
            where: {
                id: req.params.id
            }
        })
        console.log("updated profile")
        res.redirect(`profile/${players.id}`)
    } catch (err) {
        next(err);
    }
}

const destroy = async (req, res, next) => {
    try{
        const player = await players.destroy({
            where: {
                id: req.params.id
            }
        })
        console.log("deleted profile")
        res.redirect(`/`)
    } catch (err) {
        next(err);
    }
}


module.exports = {
    register: async (req, res) => {
        const {name, username, email, password} = req.body

        // const player = await players.create({
        //     name, username, email, password
        // });

        // const bio = await players_biodata.create({
        //     players.id = 
        // })
        players.create({name, username, email, password}).then(players=>{
            console.log(players)
            res.redirect(`profile/${players.id}`)
        })
    },
    profile,
    updateForm,
    update,
    destroy,
    login: passport.authenticate('local', {
        successRedirect: `profile/${players.id}`,
        failureRedirect: '/login',
        failureFlash: true
    })
}